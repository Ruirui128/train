
FROM python:3.7.0

ADD . /code

WORKDIR /code

RUN pip install -i https://pypi.douban.com/simple --upgrade pip
RUN pip install -r requirements.txt -i https://pypi.douban.com/simple 
CMD ["python", "countTest.py"]

